# Add a project owned by the user
resource "gitlab_project" "sample_project" {
    name = "Terraform-example-${count.index}"
    count = "${var.number_of_repos}"
    # name = "${var.name[count.index]}"
}